
# Gestionnaire de plugin Zap
# installer en suivant les indication ici :
# zsh <(curl -s https://raw.githubusercontent.com/zap-zsh/zap/master/install.zsh) --branch release-v1
# voir : https://www.zapzsh.com/ et https://github.com/zap-zsh/zap
[ -f "${XDG_DATA_HOME:-$HOME/.local/share}/zap/zap.zsh" ] && source "${XDG_DATA_HOME:-$HOME/.local/share}/zap/zap.zsh"

# Plugins
plug "zsh-users/zsh-autosuggestions"
plug "zap-zsh/supercharge"
plug "zsh-users/zsh-syntax-highlighting"
# plug "zsh-users/zsh-history-substring-search" # Pas vraiment utile avec fzf

# Load shortcut aliases
[ -f "$HOME/.config/shortcutrc" ] && source "$HOME/.config/shortcutrc"

# Load aliases
[ -f "$HOME/.config/aliasrc" ] && source "$HOME/.config/aliasrc"


# Load and initialise completion system
autoload -Uz compinit
compinit

setopt HIST_SAVE_NO_DUPS         # Do not write a duplicate event to the history file.
stty stop undef	# Disable ctrl-s to freeze terminal.
setopt interactive_comments # on peut mettre des commentaire dans la ligne de commande

# emplacement de l'historique
HISTFILE="${XDG_CACHE_HOME:-$HOME/.cache}/zsh/history"

# Completion
compdef config=git

# Basic auto/tab complete:
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)		# Include hidden files.

# FZF
source /usr/share/fzf/completion.zsh
source /usr/share/fzf/key-bindings.zsh

# Banière de bienvenue (police : ANSI Shadow)
if [ "$(tput cols)" -gt 75 ]; then
echo ""
echo " ██████╗ ██╗███████╗███╗   ██╗██╗   ██╗███████╗███╗   ██╗██╗   ██╗███████╗"
echo " ██╔══██╗██║██╔════╝████╗  ██║██║   ██║██╔════╝████╗  ██║██║   ██║██╔════╝"
echo " ██████╔╝██║█████╗  ██╔██╗ ██║██║   ██║█████╗  ██╔██╗ ██║██║   ██║█████╗  "
echo " ██╔══██╗██║██╔══╝  ██║╚██╗██║╚██╗ ██╔╝██╔══╝  ██║╚██╗██║██║   ██║██╔══╝  "
echo " ██████╔╝██║███████╗██║ ╚████║ ╚████╔╝ ███████╗██║ ╚████║╚██████╔╝███████╗"
echo " ╚═════╝ ╚═╝╚══════╝╚═╝  ╚═══╝  ╚═══╝  ╚══════╝╚═╝  ╚═══╝ ╚═════╝ ╚══════╝"


else
	echo -e "\t╔══════════════╗"
	echo -e "\t║  Bienvenue ! ║"
	echo -e "\t╚══════════════╝"
fi
#lecture de os-release (pour récup du nom de l'OS)
. /etc/os-release

echo ""
echo -e "\tOS................ : ${PRETTY_NAME}"
echo -e "\tHostname.......... : ${HOST}"
echo -e "\tUptime............ : $(uptime -p)"
echo -e "\tProcesses......... : $(ps -e | wc -l) total running wich $(ps -u $USER | wc -l) are yours"
echo -e "\tUsers logged...... : $(users | tr ' ' '\n' | sort -u)" # ajouter | wc -l pour avoir juste le nombre

#--------------------------------------------------------------------#
#                              VIM etc.                              #
#--------------------------------------------------------------------#
# Principalement tiré de https://thevaluable.dev/zsh-install-configure-mouseless/
# et de https://github.com/Phantas0s/.dotfiles

bindkey -v # active vim mode
export KEYTIMEOUT=1 # switch plus rapidement entre les modes

# fonction pour switcher la forme du curseur
cursor_mode() {
    # See https://ttssh2.osdn.jp/manual/4/en/usage/tips/vim.html for cursor shapes
    cursor_block='\e[2 q'
    cursor_beam='\e[6 q'

    function zle-keymap-select {
        if [[ ${KEYMAP} == vicmd ]] ||
            [[ $1 = 'block' ]]; then
            echo -ne $cursor_block
        elif [[ ${KEYMAP} == main ]] ||
            [[ ${KEYMAP} == viins ]] ||
            [[ ${KEYMAP} = '' ]] ||
            [[ $1 = 'beam' ]]; then
            echo -ne $cursor_beam
        fi
    }

    zle-line-init() {
        echo -ne $cursor_beam
    }


    zle -N zle-keymap-select
    zle -N zle-line-init
}

cursor_mode



# vim key pour naviguer dans la complétition
zmodload zsh/complist
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history

# Editer la ligne direct dans vim ($EDITOR)
autoload -Uz edit-command-line
zle -N edit-command-line
bindkey -M vicmd v edit-command-line # appuyer sur v en mode normal pour activer

# text objects (da" ci" etc.)

autoload -Uz select-bracketed select-quoted
zle -N select-quoted
zle -N select-bracketed
for km in viopp visual; do
  bindkey -M $km -- '-' vi-up-line-or-history
  for c in {a,i}${(s..)^:-\'\"\`\|,./:;=+@}; do
    bindkey -M $km $c select-quoted
  done
  for c in {a,i}${(s..)^:-'()[]{}<>bB'}; do
    bindkey -M $km $c select-bracketed
  done
done

# Se déplacer d'un mot pour l'aztocomplétition
bindkey "^[[1;3C" forward-word
bindkey "^[[1;3D" backward-word

# Correction bug delete, home, end
bindkey  "^[[H"   beginning-of-line
bindkey  "^[[F"   end-of-line
bindkey  "^[[3~"  delete-char



# Prompt
eval "$(starship init zsh)"

# INFO: Modeline pour dire que c'est du bash, y a pas de TreeSitter pour Zsh
# vim: set ft=bash:
