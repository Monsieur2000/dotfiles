# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# Attention ! s'assurer que .xsessionrc est un symlink de ce fichier ou lit ce fichier
# Certaines distros ne lisent pas .profil quand elle ouvrent une session graphique. Du coup, on peut aussi le symlinker en .bash_profile


# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin and all sub folders if it exists
if [ -d "$HOME/.local/bin" ] ; then
    #export PATH="$PATH:$(du "$HOME/.local/bin/" | cut -f2 | tr '\n' ':' | sed 's/:*$//')"
    export PATH="$PATH:$(find -L "$HOME/.local/bin" -type d | tr '\n' ':' | sed 's/:*$//')"
fi

export PATH=~/.npm-global/bin:$PATH
# variables environnementales perso, principalement pour wm et scripts annexes
export BROWSER="firefox"
export TERMINAL="alacritty"
export EDITOR="nvim"
export FILE="ranger"

export GTK_THEME=Matcha-dark-sea

# On met la config de zsh dans le dossier de config
export ZDOTDIR="$HOME/.config/zsh"

export NOTES_DIR="$HOME/doc/tdo" # pour le logiciel tdo https://github.com/2kabhishek/tdo

# less/man colors - less est le PAGER par defaut qui lit les pages man.
# ATTENTION y a des caractères bizares qui posent des problèmes en copié/collé dans vim
export LESS=-R
export LESS_TERMCAP_mb="$(printf '%b' '[1;31m')"; a="${a%_}"
export LESS_TERMCAP_md="$(printf '%b' '[1;36m')"; a="${a%_}"
export LESS_TERMCAP_me="$(printf '%b' '[0m')"; a="${a%_}"
export LESS_TERMCAP_so="$(printf '%b' '[01;44;33m')"; a="${a%_}"
export LESS_TERMCAP_se="$(printf '%b' '[0m')"; a="${a%_}"
export LESS_TERMCAP_us="$(printf '%b' '[1;32m')"; a="${a%_}"
export LESS_TERMCAP_ue="$(printf '%b' '[0m')"; a="${a%_}"

# CONFIG FZF
export FZF_DEFAULT_OPTS=$FZF_DEFAULT_OPTS'
--color=dark
--color=fg:-1,bg:-1,hl:#5fff87,fg+:-1,bg+:-1,hl+:#ffaf5f
--color=info:#af87ff,prompt:#5fff87,pointer:#ff87d7,marker:#ff87d7,spinner:#ff87d7
'
