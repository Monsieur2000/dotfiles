# Mes dotfiles

Le système de gestion est basé sur l'utilisation d'un dépôt 'bare' git qui surveille tout le $HOME.

J'ai suivi ce guide : https://www.atlassian.com/git/tutorials/dotfiles

## Les fichiers de login

.xinitrc
:   lu par startx

.bashrc
:   lu si bash n'est pas un login prompt (genre si on ouvre un terminal dans un WM)

.bash_profile
:   lu si bash est un login prompt (genre celui des tty qu'on lance avec ctrl+alt+F* ).

.profile
:   lu par n'importe quel shell mais pas par bash si il existe un .bash_profile

.xprofile
:   lu par les "displays managers" (DM)


### Utilisation dans ces dotfiles

1. .bash_profile test si il y a un .profile, si oui il est inclut.
2. Dans .profile, il y a un test pour voir si'il y a un .bashrc et l'inclut si c'est le cas.

## Git
### Installation

Pour éviter que le clonage du dépôt ne fasse des erreurs si des fichiers existent déjà, on utilise le script `deploydots.sh` qui se trouve dans : `.local/bin/tools`
  1. Télécharger le script dans le `$HOME`
  2. Modifier les chemins
  3. Executer `deploydots.sh`


### Gestion
Pour gérer le dépôt *bare git* il existe un alias dans `.config/aliasrc` :

```sh
alias config='/usr/bin/git --git-dir=$HOME/var/src/dotfiles-portable-archives/ --work-tree=$HOME'
```

Mais, il n'y pas de complétion automatique des noms de fichiers quand on fait un `config add` ou `config commit`. Pour y remédier, il existe deux scripts dans `.local/bin/tools` : `config-add` et `config-commit`.
