#!/bin/sh

# Script pour cloner mon dépôt de dotfiles et les copier dans le $HOME.
# Fait un backup des fichiers existants en leur ajoutant .old a la fin

mkdir -p $HOME/var/src

git clone --bare https://gitlab.com/monsieur2000/dotfiles.git $HOME/var/src/dotfiles-travail
function config {
    /usr/bin/git --git-dir=$HOME/var/src/dotfiles-travail/ --work-tree=$HOME $@
}

config checkout
if [ $? = 0 ]; then
    echo "Checked out config.";
else
    echo "Backing up pre-existing dot files.";
    config checkout 2>&1 | egrep "\s+\." | awk {'print $1'} | xargs -I{} mv {} {}.old
fi;
config checkout
config config status.showUntrackedFiles no
